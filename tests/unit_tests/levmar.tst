// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->
// <-- ENGLISH IMPOSED -->


//
// assert_close --
//   Returns 1 if the two real matrices computed and expected are close,
//   i.e. if the relative distance between computed and expected is lesser than epsilon.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
function flag = assert_close ( computed, expected, epsilon )
  if expected==0.0 then
    shift = norm(computed-expected);
  else
    shift = norm(computed-expected)/norm(expected);
  end
  if shift < epsilon then
    flag = 1;
  else
    flag = 0;
  end
  if flag <> 1 then pause,end
endfunction
//
// assert_equal --
//   Returns 1 if the two real matrices computed and expected are equal.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
function flag = assert_equal ( computed , expected )
  if computed==expected then
    flag = 1;
  else
    flag = 0;
  end
  if flag <> 1 then pause,end
endfunction


// A data fitting problem
M = 100;
N = 2;
// The function to approximate
// x: the input
// p: the parameter
// y: the output
function y = fapprox ( x , p )
  y = sin(p(1)*x) + sin(p(2)*x)
endfunction
// The data to fit
datax = linspace ( 0 , 4 , M )';
noisevar = 0.2;
noise = (2*rand ( M , 1 ) - 1 ) * noisevar;

datay = fapprox ( datax , [2 3]) + noise;
if ( %f ) then
  plot ( datax , datay , "b*" )
end

// Returns the residual, a column vector of size M = 100.
// x: the parameter of the model
function r = residual ( p )
  r = datay - fapprox ( datax , p )
endfunction


function [f, g, jac] = costfun ( x )
  r = residual ( x )
  f = 0.5 * r' * r
  jac = numderivative ( residual , x )
  g = jac' * r
endfunction

if ( %f ) then
  [f, g, jac] = costfun ( [2 3]' )
end

// Test with default parameters
x0 = [1.5 2]';
MaxIt = 100;
Tol = 1e-6;
[xopt, histout, costdata] = optkelley_levmar ( x0, costfun, Tol, MaxIt);
niter = size(histout,"r");
fopt = histout(niter,2);
assert_close ( xopt , [2 3]', 1.e-1 );
assert_equal ( fopt < 3 , %t );

// See the result
if ( %f ) then
  dataz = fapprox ( datax , xopt );
  plot ( datax , dataz , "g-" )
end


