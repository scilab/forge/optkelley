mode(1)
//
// Demo of optkelley_cgtrust.sci
//

function [f,g] = rosenbrock ( x )
f = 100.0 *(x(2)-x(1)^2)^2 + (1-x(1))^2;
g(1) = - 400. * ( x(2) - x(1)**2 ) * x(1) -2. * ( 1. - x(1) )
g(2) = 200. * ( x(2) - x(1)**2 )
endfunction
halt()   // Press return to continue

// With default parameters
x0 = [-1.2 1.0]';
[xopt, histout, costdata] = optkelley_cgtrust(x0, rosenbrock);
xopt
histout
costdata
niter = size(histout,"r")
fopt = histout(niter,2)
halt()   // Press return to continue

// Customize parameters
// Set tol
[xopt, histout, costdata] = optkelley_cgtrust(x0, rosenbrock, 0);
// Set tol and eta
[xopt, histout, costdata] = optkelley_cgtrust(x0, rosenbrock, [1e-6 0.5]);
// Set tol, eta and maxit
[xopt, histout, costdata] = optkelley_cgtrust(x0, rosenbrock, [1e-6 0.1 22]);
// Set tol, eta, maxit and maxitl
[xopt, histout, costdata] = optkelley_cgtrust(x0, rosenbrock, [1e-6 0.1 50 1]);
// Use default params and set resolution
[xopt, histout, costdata] = optkelley_cgtrust(x0, rosenbrock, [], 1e-12);
// Use default params and resolution, set verbose
[xopt, histout, costdata] = optkelley_cgtrust(x0, rosenbrock, [], [] , %t );
// Configure all parameters
[xopt, histout, costdata] = optkelley_cgtrust(x0, rosenbrock, [1e-6 0.1 1000 100], 1e-12 , %t );
halt()   // Press return to continue

// Other ways of using the default parameters
[xopt, histout, costdata] = optkelley_cgtrust(x0, rosenbrock );
[xopt, histout, costdata] = optkelley_cgtrust(x0, rosenbrock , [] );
[xopt, histout, costdata] = optkelley_cgtrust(x0, rosenbrock , [] , [] );
[xopt, histout, costdata] = optkelley_cgtrust(x0, rosenbrock , [] , [] , [] );
halt()   // Press return to continue

//========= E N D === O F === D E M O =========//
//
// Load this script into the editor
//
filename = "optkelley_cgtrust.sce";
dname = get_absolute_file_path(filename);
editor ( dname + filename );
