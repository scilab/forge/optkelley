function H = optkelley_diffhess(x, f, gc, heps)
  // Compute a forward difference Hessian.
  //
  // Calling Sequence
  //  H = optkelley_diffhess(x, f, gc)
  //  H = optkelley_diffhess(x, f, gc, heps)
  //
  // Parameters
  //  x: point
  //  f: function. The calling sequence for the function is [func,grad]=f(x).
  //  gc: current gradient at point x
  //  heps: a 1x1 matrix of doubles, the step, optional, default = 1.d-6
  //  H hessian
  //		
  // Description
  //   Approximates f''(x).
  //   This function uses optkelley_dirdero to compute the columns, then symmetrize.
  //
  //   This code comes with no guarantee or warranty of any kind.
  //
  // Examples
  // function [f,g] = rosenbrock ( x )
  //   f = 100.0 *(x(2)-x(1)^2)^2 + (1-x(1))^2;
  //   g(1) = - 400. * ( x(2) - x(1)**2 ) * x(1) -2. * ( 1. - x(1) )
  //   g(2) = 200. * ( x(2) - x(1)**2 )
  // endfunction
  // function H = roseHess ( x )
  //   H(1,1) = 1200 * x(1)**2 - 400 * x(2) + 2
  //   H(1,2) = -400 * x(1)
  //   H(2,1) = H(1,2)
  //   H(2,2) = 200
  // endfunction
  // //
  // // Test with default heps
  // x0 = [-1.2 1.0]';
  // [f0,g0] = rosenbrock ( x0 );
  // Hcomputed = optkelley_diffhess(x0, rosenbrock, g0)
  // // Compare with exact Hessian
  // Hexpected = roseHess ( x0 )
  //
  // // Test with manual heps
  // x0 = [-1.2 1.0]';
  // w = [1 0]';
  // [f0,g0] = rosenbrock ( x0 );
  // Hcomputed = optkelley_diffhess(x0, rosenbrock, g0, 1.e-8)
  // // Compare with exact Hessian
  // Hexpected = roseHess ( x0 )
  //
  // Authors
  // C. T. Kelley, March 17, 1998
  // Yann Collette, Scilab port
  // Michael Baudin, DIGITEO, 2010, Update to Scilab 5.2, formatted comments, added tests and example
  //

  //
  [nargout, nargin] = argn();
  if (nargin == 3) then
    heps = 1.d-6;
  end
  n=length(x);
  for j=1:n
    zz    = zeros(n,1);
    zz(j) = 1;
    H(:,j)=optkelley_dirdero(x,zz,f,gc,heps);
  end
  //
  // symmetrize
  //
  H = (H+H')*.5;
endfunction

