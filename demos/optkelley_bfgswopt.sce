mode(1)
//
// Demo of optkelley_bfgswopt.sci
//

function [f,g] = rosenbrock ( x )
f = 100.0 *(x(2)-x(1)^2)^2 + (1-x(1))^2;
g(1) = - 400. * ( x(2) - x(1)**2 ) * x(1) -2. * ( 1. - x(1) )
g(2) = 200. * ( x(2) - x(1)**2 )
endfunction
x0 = [-1.2 1.0]';
maxit     = 1000;
tol       = 1e-6;
[xopt,histout,costdata] = optkelley_bfgswopt(x0,rosenbrock,tol,maxit);
xopt
histout
costdata
niter = size(histout,"r");
fopt = histout(niter,2)
halt()   // Press return to continue

// Set maxit, let tol to the default.
[xopt,histout,costdata] = optkelley_bfgswopt(x0,rosenbrock,[],1000);
halt()   // Press return to continue

// Set maxit, set verbose, let other parameters to the default.
[xopt,histout,costdata] = optkelley_bfgswopt(x0,rosenbrock,[],1000,[],%t);
halt()   // Press return to continue

// Provide the effect of the inverse Hessian
function h0v = roseHess0 ( v )
x = [-1.2 1.0]'
H(1,1) = 1200 * x(1)**2 - 400 * x(2) + 2
H(1,2) = -400 * x(1)
H(2,1) = H(1,2)
H(2,2) = 200
h0v = inv(H) * v
endfunction
[xopt,histout,costdata] = optkelley_bfgswopt(x0,rosenbrock,[],1000,roseHess0);
halt()   // Press return to continue

//========= E N D === O F === D E M O =========//
//
// Load this script into the editor
//
filename = "optkelley_bfgswopt.sce";
dname = get_absolute_file_path(filename);
editor ( dname + filename );
