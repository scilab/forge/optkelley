function [sgr,fb,xb,sflag]=optkelley_simpgrad(x,f,v,fc,fdiff)
// Simplex gradient.
//
// Calling Sequence
//   [sgr,fb,xb,sflag]=optkelley_simpgrad(x,f,v,fc,fdiff)
//
// Parameters
//   x: current point
//   f: function with calling sequence y = f(x)
//   v: simplex, where v(:,j) is the j-th vertex
//   fdiff: 1 to get forward differencing. Useful in Nelder-Mead simplex condition/gradient computation. Omit fdiff or set to 0 in typical implicit filtering mode
//   sgr: simplex gradient
//   fb: best value in stencil
//   xb: best point in stencil
//   sflag: 0 if (1) you're using central diffs and, (2) center of stencil is best point. sflag is used to detect stencil failure
//
// Description
//   Compute the simplex gradient for use with implicit filtering.
//   Also tests for best point in stencil.
//
// Authors
// C. T. Kelley
// Yann Collette, Scilab port
// Michael Baudin, DIGITEO, 2010, Update to Scilab 5.2, formatted comments

//
//
n     = length(x);
delp  = zeros(n,1);
delm  = zeros(n,1);
xb    = x;
fb    = fc;
sflag = 0;
for j=1:n
  xp = x+v(:,j);
  xm = x-v(:,j);
  fp = f(xp);
  delp(j) = fp-fc;
  if (fp < fb) then
    fb    = fp;
    xb    = xp;
    sflag = 1;
  end
  if (fdiff==0) then
    fm      = f(xm);
    delm(j) = fc-fm;
  end
  if (fm < fb) then
    fb    = fm;
    xb    = xm;
    sflag = 1;
  end
end
//end
if (fdiff==1) then
  sgr = v'\delp;
else
  sgr = .5*((v'\delp)+(v'\delm));
end
if (fdiff==1) then
  xb    = x;
  fb    = fc;
  sflag = 1;
end
endfunction

