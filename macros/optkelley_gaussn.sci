function [x,histout,costdata] = optkelley_gaussn ( varargin )
  // Damped Gauss-Newton with Armijo rule.
  //
  // Calling Sequence
  //   [x,histout,costdata] = optkelley_gaussn ( x0 , f )
  //   [x,histout,costdata] = optkelley_gaussn ( x0 , f , tol )
  //   [x,histout,costdata] = optkelley_gaussn ( x0 , f , tol ,maxit )
  //   [x,histout,costdata] = optkelley_gaussn ( x0 , f , tol ,maxit , verbose )
  //
  // Parameters
  //   x0: initial iterate
  //   f: r^T r/2 = objective function, the calling sequence for f should be [fout,gout,jac]=f(x) where fout=f(x) is a scalar, gout = jac^T r = grad f(x) is a COLUMN vector and jac = r' = Jacobian of r is an M x N matrix
  //   tol: (optional) termination criterion norm(grad) < tol, default = 1.e-6. If tol==[], then the default is used.
  //   maxit: (optional) maximum iterations, default = 100. If maxit==[], then the default is used.
  //   verbose: (optional) if true, print messages (default=%f). If verbose==[], the default parameter is used.
  //   x: solution
  //   histout: iteration history Each row of histout is [norm(grad), f, number of step length reductions, iteration count]
  //   costdata: [num f, num grad, num hess] (for gaussn, num hess=0)
  //
  // Description
  // Simple divide by 2 stepsize reduction.
  // This code comes with no guarantee or warranty of any kind.
  //
  // At this stage all iteration parameters are hardwired in the code.
  //
  // Examples
  //  // A data fitting problem
  //  M = 100;
  //  N = 2;
  //  // The function to approximate
  //  // x: the input
  //  // p: the parameter
  //  // y: the output
  //  function y = fapprox ( x , p )
  //    y = sin(p(1)*x) + sin(p(2)*x)
  //  endfunction
  //  // The data to fit
  //  rand("seed",0)
  //  datax = linspace ( 0 , 4 , M )';
  //  noisevar = 0.2;
  //  noise = (2*rand ( M , 1 ) - 1 ) * noisevar;
  //  //
  //  exacty = fapprox ( datax , [2 3]);
  //  datay = exacty + noise;
  //  plot ( datax , exacty , "r-" );
  //  plot ( datax , datay , "b*" );
  //  legend ( [ "Exact" , "Data" ] );
  //  // Returns the residual, a column vector of size M = 100.
  //  // x: the parameter of the model
  //  function r = residual ( p )
  //    r = datay - fapprox ( datax , p )
  //  endfunction
  //  function [f, g, jac] = costfun ( x )
  //    r = residual ( x )
  //    f = 0.5 * r' * r
  //    jac = derivative ( residual , x )
  //    g = jac' * r
  //  endfunction
  //  //
  //  [f, g, jac] = costfun ( [2 3]' )
  //  //
  //  // Test with default parameters
  //  x0 = [1.5 2]';
  //  [xopt, histout, costdata] = optkelley_gaussn ( x0, costfun );
  //  xopt
  //  histout
  //  costdata
  //  niter = size(histout,"r")
  //  fopt = histout(niter,2)
  //  // Plot the model
  //  dataz = fapprox ( datax , xopt );
  //  plot ( datax , dataz , "g-" )
  //  legend ( [ "Exact" , "Data" , "Model" ] );
  //
  //  // Set tol
  //  [xopt, histout, costdata] = optkelley_gaussn ( x0, costfun, 0 );
  //  // Use default tol and set maxit
  //  [xopt, histout, costdata] = optkelley_gaussn ( x0, costfun, [] , 5 );
  //  // Use default tol, maxit and set verbose
  //  [xopt, histout, costdata] = optkelley_gaussn ( x0, costfun, [] , [] , %t );
  //
  //  // Other ways of using the default parameters
  //  [xopt, histout, costdata] = optkelley_gaussn ( x0, costfun );
  //  [xopt, histout, costdata] = optkelley_gaussn ( x0, costfun , [] );
  //  [xopt, histout, costdata] = optkelley_gaussn ( x0, costfun , [] , [] );
  //  [xopt, histout, costdata] = optkelley_gaussn ( x0, costfun , [] , [] , [] );
  //
  // Authors
  // C. T. Kelley, Dec 14, 1997
  // Yann Collette, Scilab port
  // Michael Baudin, DIGITEO, 2010, Update to Scilab 5.2, formatted comments, added tests and example, improved management of input arguments
  //

  //
  [lhs, rhs] = argn();
  if ( rhs < 2 | rhs > 5 ) then
    errmsg = msprintf(gettext("%s: Unexpected number of input arguments : %d provided while 2 to 5 are expected."), "optkelley_gaussn", rhs);
    error(errmsg)
  end
  //
  x0 = varargin(1)
  f = varargin(2)
  tol = 1.e-6
  if ( rhs >= 3 & varargin(3) <> [] ) then
    tol = varargin(3)
  end
  maxit = 100;
  if ( rhs >= 4 & varargin(4) <> [] ) then
    maxit = varargin(4)
  end
  verbose = %f
  if ( rhs >= 5 & varargin(5) <> [] ) then
    verbose = varargin(5)
  end
  //
  alp = 1.d-4;
  itc = 1;
  xc  = x0;
  [fc,gc,jac] = f(xc);
  numf = 1;
  numg = 1;
  numh = 0;
  ithist = zeros(1,4);
  ithist(1,1) = norm(gc);
  ithist(1,2) = fc;
  ithist(1,4) = itc-1;
  ithist(1,3) = 0;
  while(norm(gc) > tol & itc < maxit)
    dc = (jac'*jac)\gc;
    lambda = 1.0;
    xt     = xc-lambda*dc;
    ft     = f(xt);
    numf   = numf+1;
    iarm   = 0;
    itc    = itc+1;
    //
    // Goal for sufficient decrease
    //
    fgoal = fc - alp*lambda*(gc'*dc);
    while(ft > fgoal)
      iarm   = iarm+1;
      lambda = lambda/2;
      xt     = xc-lambda*dc;
      ft     = f(xt);
      numf   = numf+1;
      if (iarm > 10) then
        if ( verbose ) then
          mprintf("%s: Armijo error in Gauss-Newton\n","optkelley_gaussn")
        end
        x        = xc;
        histout  = ithist(1:itc-1,:);
        costdata = [numf, numg, numh];
        return;
      end
    end
    xc = xt;
    [fc,gc,jac] = f(xc);
    numf        = numf+1;
    numg        = numg+1;
    if ( verbose ) then
      mprintf("it=%d, g=%15.5e, f=%15.5e, iarm=%d\n",itc-1,norm(gc), fc, iarm)
    end
    ithist(itc,1:4) = [norm(gc) fc itc-1 iarm];
  end
  x = xc;
  histout  = ithist(1:itc,:);
  costdata = [numf, numg, numh];
endfunction

