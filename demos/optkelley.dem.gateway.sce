// This file is released into the public domain
// This help file was generated using helpupdate at 2010/6/2 - 15:30:51
demopath = get_absolute_file_path("optkelley.dem.gateway.sce");
subdemolist = [
"steep2", "steep2.sce"; ..
"steep", "steep.sce"; ..
"runtests", "runtests.sce"; ..
"optkelley_steep", "optkelley_steep.sce"; ..
"optkelley_simpgrad", "optkelley_simpgrad.sce"; ..
"optkelley_projbfgs", "optkelley_projbfgs.sce"; ..
"optkelley_polymod", "optkelley_polymod.sce"; ..
"optkelley_polyline", "optkelley_polyline.sce"; ..
"optkelley_ntrust", "optkelley_ntrust.sce"; ..
"optkelley_nelder", "optkelley_nelder.sce"; ..
"optkelley_mds", "optkelley_mds.sce"; ..
"optkelley_levmar", "optkelley_levmar.sce"; ..
"optkelley_imfil", "optkelley_imfil.sce"; ..
"optkelley_hooke", "optkelley_hooke.sce"; ..
"optkelley_gradproj", "optkelley_gradproj.sce"; ..
"optkelley_gaussn", "optkelley_gaussn.sce"; ..
"optkelley_dirdero", "optkelley_dirdero.sce"; ..
"optkelley_diffhess", "optkelley_diffhess.sce"; ..
"optkelley_cgtrust", "optkelley_cgtrust.sce"; ..
"optkelley_bfgswopt", "optkelley_bfgswopt.sce"; ..
"ntrust", "ntrust.sce"; ..
"nelder", "nelder.sce"; ..
"mds2", "mds2.sce"; ..
"mds", "mds.sce"; ..
"Kelleydemo", "Kelleydemo.sce"; ..
"bfgs2", "bfgs2.sce"; ..
"bfgs", "bfgs.sce"; ..
];
subdemolist(:,2) = demopath + subdemolist(:,2)
