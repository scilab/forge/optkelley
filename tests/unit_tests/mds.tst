// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->
// <-- ENGLISH IMPOSED -->


//
// assert_close --
//   Returns 1 if the two real matrices computed and expected are close,
//   i.e. if the relative distance between computed and expected is lesser than epsilon.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
function flag = assert_close ( computed, expected, epsilon )
  if expected==0.0 then
    shift = norm(computed-expected);
  else
    shift = norm(computed-expected)/norm(expected);
  end
  if shift < epsilon then
    flag = 1;
  else
    flag = 0;
  end
  if flag <> 1 then pause,end
endfunction
//
// assert_equal --
//   Returns 1 if the two real matrices computed and expected are equal.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
function flag = assert_equal ( computed , expected )
  if computed==expected then
    flag = 1;
  else
    flag = 0;
  end
  if flag <> 1 then pause,end
endfunction
function y = quad ( x )
  x1 = [1 1]'
  x2 = [2 2]'
  y = max ( norm(x-x1) , norm(x-x2) )
endfunction

//
// Test basic use with parameters
//
x0 = [0 0]';
v1 = x0 + [0.1 0]';
v2 = x0 + [0 0.1]';
v0 = [x0 v1 v2];
[x,lhist,histout] = optkelley_mds(v0,quad,1.e-4,100,100);
xopt = x(:,1);
fopt = histout(lhist,2);
assert_close ( xopt , [1.5   1.5]', 1e-2 );
assert_close ( fopt , 0.70710678118654757 , 1e-4 );

//
// Test with default budget
//
x0 = [0 0]';
v1 = x0 + [0.1 0]';
v2 = x0 + [0 0.1]';
v0 = [x0 v1 v2];
[x,lhist,histout] = optkelley_mds(v0,quad,1.e-4,100);
xopt = x(:,1);
fopt = histout(lhist,2);
assert_close ( xopt , [1.5   1.5]', 1e-2 );
assert_close ( fopt , 0.70710678118654757 , 1e-4 );

//
// Test with default budget + default maxit
//
x0 = [0 0]';
v1 = x0 + [0.1 0]';
v2 = x0 + [0 0.1]';
v0 = [x0 v1 v2];
[x,lhist,histout] = optkelley_mds(v0,quad,1.e-4);
xopt = x(:,1);
fopt = histout(lhist,2);
assert_close ( xopt , [1.5   1.5]', 1e-2 );
assert_close ( fopt , 0.70710678118654757 , 1e-4 );

