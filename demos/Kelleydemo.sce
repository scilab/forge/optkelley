// Copyright (C) - Yann Collette
// Copyright (C) 2010 - DIGITEO - Michael Baudin
// TestKelley.sce

function [f,g] = rosenbrock ( x )
  f = 100.0 *(x(2)-x(1)^2)^2 + (1-x(1))^2;
  g(1) = - 400. * ( x(2) - x(1)**2 ) * x(1) -2. * ( 1. - x(1) )
  g(2) = 200. * ( x(2) - x(1)**2 )
endfunction
function y = rosenbrockC ( x1 , x2 )
  x = [x1 x2]
  y = 100.0 *(x(2)-x(1)^2)^2 + (1-x(1))^2;
endfunction

x = linspace ( -2, 2 , 100 );
y = linspace ( -2, 2 , 100 );

MaxIt     = 1000;
Tol       = 1e-6;
GradTol   = 1e-12;

//
// Display the cost function
//
function [f, g, jac] = rosenbrock_2(x)
  f = rosenbrock(x);
  [g, jac] = numderivative(rosenbrock, x);
  g = g';
endfunction

x0 = [-1.2 1.0]';

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Title = "Optkelley demo"
xtitle(Title + " function - optim_kelley_bfgswopt","x1","x2");

printf("optim_kelley_bfgswopt:\n");
printf("steepest descent/bfgs with polynomial line search\n");
printf(" and Steve Wright storage of H^-1\n");

f = scf(100001);
contour ( x , y , rosenbrockC ,  [1 10 100 500 1000] )
plot(x0(1), x0(2), "ro");

[x_opt, histout, costdata] = optkelley_bfgswopt(x0, rosenbrock, Tol, MaxIt);

plot(x_opt(1), x_opt(2), "go");

printf("optkelley_bfgswopt: Starting point x0:"); disp(x0);
printf("optkelley_bfgswopt: Final point x_opt:"); disp(x_opt);
printf("optkelley_bfgswopt: Initial fobj = %f - Final fobj = %f\n", rosenbrock(x0), rosenbrock(x_opt));

mprintf("\n================================\nClick on the graphics to continue\n");
xclick();
clf();

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

xtitle(Title + " function - cgtrust","x1","x2");

printf("optkelley_cgtrust:\n");
printf("Steihaug Newton-CG-Trust region algorithm\n")

xset("fpf"," ");
contour ( x , y , rosenbrockC ,  [1 10 100 500 1000] )

plot(x0(1), x0(2), "ro");

[x_opt, histout, costdata] = optkelley_cgtrust(x0, rosenbrock, [Tol, 0.1, MaxIt, ceil(0.1*MaxIt)], GradTol);

plot(x_opt(1), x_opt(2), "go");

printf("optkelley_cgtrust: Starting point x0:"); disp(x0);
printf("optkelley_cgtrust: Final point x_opt:"); disp(x_opt);
printf("optkelley_cgtrust: Initial fobj = %f - Final fobj = %f\n", rosenbrock(x0), rosenbrock(x_opt));

mprintf("\n================================\nClick on the graphics to continue\n");
xclick();
clf();

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// A data fitting problem
M = 100;
N = 2;
// The function to approximate
// x: the input
// p: the parameter
// y: the output
function y = fapprox ( x , p )
  y = sin(p(1)*x) + sin(p(2)*x)
endfunction
// The data to fit
datax = linspace ( 0 , 4 , M )';
noisevar = 0.2;
noise = (2*rand ( M , 1 ) - 1 ) * noisevar;

datay = fapprox ( datax , [2 3]) + noise;

// Returns the residual, a column vector of size M = 100.
// x: the parameter of the model
function r = residual ( p )
  r = datay - fapprox ( datax , p )
endfunction


function [f, g, jac] = costfun ( x )
  x = x(:)
  r = residual ( x )
  f = 0.5 * r' * r
  jac = numderivative ( residual , x )
  g = jac' * r
endfunction


function f = costfunC ( x1 , x2 )
  x = [x1 x2]
  x = x(:)
  r = residual ( x )
  f = 0.5 * r' * r
endfunction
x0R = [1.5 2]';

xtitle(Title + " function - optkelley_gaussn","x1","x2");

printf("optkelley_gaussn:\n");
printf("Damped Gauss-Newton with Armijo rule\n");
printf("simple divide by 2 stepsize reduction\n");

xset("fpf"," ");
xR = linspace ( 1, 3 , 100 );
yR = linspace ( 1, 4 , 100 );
contour ( xR , yR , costfunC , 10 )

plot(x0R(1), x0R(2), "ro");

[x_opt, histout, costdata] = optkelley_gaussn(x0R, costfun, Tol, MaxIt);

plot(x_opt(1), x_opt(2), "go");

printf("optkelley_gaussn: Starting point x0:"); disp(x0R);
printf("optkelley_gaussn: Final point x_opt:"); disp(x_opt);
printf("optkelley_gaussn: Initial fobj = %f - Final fobj = %f\n", costfun(x0R), costfun(x_opt));

mprintf("\n================================\nClick on the graphics to continue\n");
xclick();
clf();

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

xtitle(Title + " function - optkelley_gradproj","x1","x2");

printf("optkelley_gradproj:\n");
printf("gradient projection with Armijo rule, simple linesearch\n");

xset("fpf"," ");
contour ( x , y , rosenbrockC ,  [1 10 100 500 1000] )

plot(x0(1), x0(2), "ro");

[x_opt, histout, costdata] = optkelley_gradproj(x0, rosenbrock, [0.5 2], [-2 -2], Tol, MaxIt);

plot(x_opt(1), x_opt(2), "go");

printf("optkelley_gradproj: Starting point x0:"); disp(x0);
printf("optkelley_gradproj: Final point x_opt:"); disp(x_opt);
printf("optkelley_gradproj: Initial fobj = %f - Final fobj = %f\n", rosenbrock(x0), rosenbrock(x_opt));

mprintf("\n================================\nClick on the graphics to continue\n");
xclick();
clf();

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

xtitle(Title + " function - optkelley_hooke","x1","x2");

printf("optkelley_hooke:\n");
printf("Nelder-Mead optimizer, No tie-breaking rule other than MATLAB""s sort\n");

xset("fpf"," ");
contour ( x , y , rosenbrockC ,  [1 10 100 500 1000] )

plot(x0(1), x0(2), "ro");

[x_opt, histout] = optkelley_hooke(x0, rosenbrock, MaxIt);

plot(x_opt(1), x_opt(2), "go");

printf("optkelley_hooke: Starting point x0:"); disp(x0);
printf("optkelley_hooke: Final point x_opt:"); disp(x_opt);
printf("optkelley_hooke: Initial fobj = %f - Final fobj = %f\n", rosenbrock(x0), rosenbrock(x_opt));

mprintf("\n================================\nClick on the graphics to continue\n");
xclick();
clf();
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

xtitle(Title + " function - optkelley_imfil","x1","x2");

printf("optkelley_imfil:\n");
printf("Unconstrained implicit filtering code\n");
printf("Implicit Filtering with SR1 and BFGS quasi-Newton methods\n");

xset("fpf"," ");
contour ( x , y , rosenbrockC ,  [1 10 100 500 1000] )

plot(x0(1), x0(2), "ro");

[x_opt, histout, costdata] = optkelley_imfil(x0, rosenbrock, MaxIt);

plot(x_opt(1), x_opt(2), "go");

printf("optkelley_imfil: Starting point x0:"); disp(x0);
printf("optkelley_imfil: Final point x_opt:"); disp(x_opt);
printf("optkelley_imfil: Initial fobj = %f - Final fobj = %f\n", rosenbrock(x0), rosenbrock(x_opt));

mprintf("\n================================\nClick on the graphics to continue\n");
xclick();
clf();
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// A data fitting problem


xtitle(Title + " function - optkelley_levmar","x1","x2");

printf("optkelley_levmar:\n");
printf("Levenberg-Marquardt code, trust region control of LM parameter\n");

x0R = [1.5 2.5]';

xset("fpf"," ");
contour ( xR , yR , costfunC ,  10 )

plot(x0R(1), x0R(2), "ro");

// Reduce the number of iterations : MaxIt/2
[x_opt, histout, costdata] = optkelley_levmar (x0R, costfun, Tol, MaxIt/2);

plot(x_opt(1), x_opt(2), "go");

printf("optkelley_levmar: Starting point x0:"); disp(x0R);
printf("optkelley_levmar: Final point x_opt:"); disp(x_opt);
printf("optkelley_levmar: Initial fobj = %f - Final fobj = %f\n", costfun(x0), costfun(x_opt));

mprintf("\n================================\nClick on the graphics to continue\n");
xclick();
clf();
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

xtitle(Title + " function - optkelley_nelder","x1","x2");

printf("optkelley_nelder:\n");
printf("Nelder-Mead optimizer, No tie-breaking rule other than MATLAB""s sort\n");

xset("fpf"," ");
contour ( x , y , rosenbrockC ,  [1 10 100 500 1000] )

v1 = x0 + [0.1 0]'
v2 = x0 + [0 0.1]'
x0_nelder = [x0 v1 v2];

plot(x0_nelder(1,:), x0_nelder(2,:), "ro");

[x_opt, histout, costdata] = optkelley_nelder(x0_nelder, rosenbrock, Tol, MaxIt);

plot(x_opt(1,:), x_opt(2,:), "go");

printf("optkelley_nelder: Starting simpled x0_nelder:"); disp(x0_nelder);
printf("optkelley_nelder: Final simplex x_opt:"); disp(x_opt);

mprintf("\n================================\nClick on the graphics to continue\n");
xclick();
clf();
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

xtitle(Title + " function - optkelley_mds","x1","x2");

printf("optkelley_mds:\n");
printf("Multidirectional search\n");

xset("fpf"," ");
contour ( x , y , rosenbrockC ,  [1 10 100 500 1000] )

plot(x0_nelder(:,1), x0_nelder(:,2), "ro");

[x_opt, histout, costdata] = optkelley_mds(x0_nelder, rosenbrock, Tol, MaxIt);

plot(x_opt(:,1), x_opt(:,2), "go");

printf("optkelley_mds: Starting simplex x0:"); disp(x0_nelder);
printf("optkelley_mds: Final simplex x_opt:"); disp(x_opt);
printf("optkelley_mds: Initial fobj = %f - Final fobj = %f\n", rosenbrock(x0), rosenbrock(x_opt));

mprintf("\n================================\nClick on the graphics to continue\n");
xclick();
clf();
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

xtitle(Title + " function - optkelley_ntrust","x1","x2");

printf("optkelley_ntrust:\n");
printf("Dogleg trust region, Newton model, dense algorithm \n");

xset("fpf"," ");
contour ( x , y , rosenbrockC ,  [1 10 100 500 1000] )

plot(x0(1), x0(2), "ro");

[x_opt, histout, costdata] = optkelley_ntrust(x0, rosenbrock, 1.e-8 , MaxIt,  1.d-12);

plot(x_opt(1), x_opt(2), "go");

printf("optkelley_ntrust: Starting point x0:"); disp(x0);
printf("optkelley_ntrust: Final point x_opt:"); disp(x_opt);
printf("optkelley_ntrust: Initial fobj = %f - Final fobj = %f\n", rosenbrock(x0), rosenbrock(x_opt));

mprintf("\n================================\nClick on the graphics to continue\n");
xclick();
clf();
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

xtitle(Title + " function - optkelley_projbfgs","x1","x2");

printf("optkelley_projbfgs:\n");
printf("optkelley_projected BFGS with Armijo rule, simple linesearch \n");

xset("fpf"," ");
contour ( x , y , rosenbrockC ,  [1 10 100 500 1000] )

plot(x0(1), x0(2), "ro");

[x_opt, histout, costdata] = optkelley_projbfgs(x0, rosenbrock, [0.5 2], [-2 -2], Tol, MaxIt);

plot(x_opt(1), x_opt(2), "go");

printf("optkelley_projbfgs: Starting point x0:"); disp(x0);
printf("optkelley_projbfgs: Final point x_opt:"); disp(x_opt);
printf("optkelley_projbfgs: Initial fobj = %f - Final fobj = %f\n", rosenbrock(x0), rosenbrock(x_opt));

mprintf("\n================================\nClick on the graphics to continue\n");
xclick();
clf();
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

xtitle(Title + " function - optkelley_steep","x1","x2");

printf("optkelley_steep:\n");
printf("steepest descent with Armijo rule, polynomial linesearch\n");

xset("fpf"," ");
contour ( x , y , rosenbrockC ,  [1 10 100 500 1000] )

plot(x0(1), x0(2), "ro");

[x_opt, histout, costdata] = optkelley_steep(x0, rosenbrock, Tol, MaxIt);

plot(x_opt(1), x_opt(2), "go");

printf("optkelley_steep: Starting point x0:"); disp(x0);
printf("optkelley_steep: Final point x_opt:"); disp(x_opt);
printf("optkelley_steep: Initial fobj = %f - Final fobj = %f\n", rosenbrock(x0), rosenbrock(x_opt));

printf("End of Demo !\n");
//
// Load this script into the editor
//
filename = 'Kelleydemo.sce';
dname = get_absolute_file_path(filename);
editor ( dname + filename );

