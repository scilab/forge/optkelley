// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->
// <-- ENGLISH IMPOSED -->


//
// assert_close --
//   Returns 1 if the two real matrices computed and expected are close,
//   i.e. if the relative distance between computed and expected is lesser than epsilon.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
function flag = assert_close ( computed, expected, epsilon )
  if expected==0.0 then
    shift = norm(computed-expected);
  else
    shift = norm(computed-expected)/norm(expected);
  end
  if shift < epsilon then
    flag = 1;
  else
    flag = 0;
  end
  if flag <> 1 then pause,end
endfunction
//
// assert_equal --
//   Returns 1 if the two real matrices computed and expected are equal.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
function flag = assert_equal ( computed , expected )
  if computed==expected then
    flag = 1;
  else
    flag = 0;
  end
  if flag <> 1 then pause,end
endfunction


function [f,g] = rosenbrock ( x )
  f = 100.0 *(x(2)-x(1)^2)^2 + (1-x(1))^2;
  g(1) = - 400. * ( x(2) - x(1)**2 ) * x(1) -2. * ( 1. - x(1) )
  g(2) = 200. * ( x(2) - x(1)**2 )
endfunction

// Test with default parameters : xopt is far from the optimum
x0 = [-1.2 1.0]';
[xopt,histout,costdata] = optkelley_bfgswopt(x0,rosenbrock);
niter = size(histout,"r");
fopt = histout(niter,2);
assert_close ( xopt , [1.   1.]', 1.e0 );
assert_equal ( fopt < 0.25 , %t );
assert_equal ( size(histout) , [21 4] );
assert_equal ( size(costdata) , [1 3] );

// Test with tol : far from optimum
x0 = [-1.2 1.0]';
tol = 0;
[xopt,histout,costdata] = optkelley_bfgswopt(x0,rosenbrock);
niter = size(histout,"r");
fopt = histout(niter,2);
assert_close ( xopt , [1.   1.]', 1.e0 );
assert_equal ( fopt < 0.25 , %t );
assert_equal ( size(histout) , [21 4] );
assert_equal ( size(costdata) , [1 3] );


// Test with tol and maxit : optimum reached
x0 = [-1.2 1.0]';
maxit = 1000;
tol = 1.e-10;
[xopt,histout,costdata] = optkelley_bfgswopt(x0,rosenbrock,tol,maxit);
niter = size(histout,"r");
fopt = histout(niter,2);
assert_close ( xopt , [1.   1.]', 1.e-8 );
assert_close ( fopt , 0 , %eps );

// Test with - no tol - and maxit : optimum reached
x0 = [-1.2 1.0]';
maxit = 1000;
[xopt,histout,costdata] = optkelley_bfgswopt(x0,rosenbrock,[],maxit);
niter = size(histout,"r");
fopt = histout(niter,2);
assert_close ( xopt , [1.   1.]', 1.e-8 );
assert_close ( fopt , 0 , %eps );


//
// Returns H0^{-1} * v
//
function h0v = roseHess0 ( v )
  x = [-1.2 1.0]'
  H(1,1) = 1200 * x(1)**2 - 400 * x(2) + 2
  H(1,2) = -400 * x(1)
  H(2,1) = H(1,2)
  H(2,2) = 200
  h0v = inv(H) * v
endfunction

// Test with tol and maxit and Hess0
x0 = [-1.2 1.0]';
maxit = 1000;
tol = 1.e-10;
[xopt,histout,costdata] = optkelley_bfgswopt(x0,rosenbrock,tol,maxit,roseHess0);
niter = size(histout,"r");
fopt = histout(niter,2);
assert_close ( xopt , [1.   1.]', 1.e-8 );
assert_close ( fopt , 0 , %eps );

// Test with tol and maxit and Hess0 and verbose
x0 = [-1.2 1.0]';
maxit = 1000;
tol = 1.e-10;
[xopt,histout,costdata] = optkelley_bfgswopt(x0,rosenbrock,tol,maxit,roseHess0,%t);
niter = size(histout,"r");
fopt = histout(niter,2);
assert_close ( xopt , [1.   1.]', 1.e-8 );
assert_close ( fopt , 0 , %eps );

// Test with tol and maxit - no Hess0 - and verbose
x0 = [-1.2 1.0]';
maxit = 1000;
tol = 1.e-10;
[xopt,histout,costdata] = optkelley_bfgswopt(x0,rosenbrock,tol,maxit,[],%t);
niter = size(histout,"r");
fopt = histout(niter,2);
assert_close ( xopt , [1.   1.]', 1.e-8 );
assert_close ( fopt , 0 , %eps );

// Enable only verbose and maxit
x0 = [-1.2 1.0]';
maxit = 1000;
[xopt,histout,costdata] = optkelley_bfgswopt(x0,rosenbrock,[],maxit,[],%t);
niter = size(histout,"r");
fopt = histout(niter,2);
assert_close ( xopt , [1.   1.]', 1.e-8 );
assert_close ( fopt , 0 , %eps );

