mode(1)
//
// Demo of optkelley_diffhess.sci
//

function [f,g] = rosenbrock ( x )
f = 100.0 *(x(2)-x(1)^2)^2 + (1-x(1))^2;
g(1) = - 400. * ( x(2) - x(1)**2 ) * x(1) -2. * ( 1. - x(1) )
g(2) = 200. * ( x(2) - x(1)**2 )
endfunction
function H = roseHess ( x )
H(1,1) = 1200 * x(1)**2 - 400 * x(2) + 2
H(1,2) = -400 * x(1)
H(2,1) = H(1,2)
H(2,2) = 200
endfunction
//
// Test with default heps
x0 = [-1.2 1.0]';
[f0,g0] = rosenbrock ( x0 );
Hcomputed = optkelley_diffhess(x0, rosenbrock, g0)
// Compare with exact Hessian
Hexpected = roseHess ( x0 )
halt()   // Press return to continue

// Test with manual heps
x0 = [-1.2 1.0]';
w = [1 0]';
[f0,g0] = rosenbrock ( x0 );
Hcomputed = optkelley_diffhess(x0, rosenbrock, g0, 1.e-8)
// Compare with exact Hessian
Hexpected = roseHess ( x0 )
halt()   // Press return to continue

//========= E N D === O F === D E M O =========//
//
// Load this script into the editor
//
filename = "optkelley_diffhess.sce";
dname = get_absolute_file_path(filename);
editor ( dname + filename );
