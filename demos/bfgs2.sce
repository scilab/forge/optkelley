// Copyright (C) 2010 - DIGITEO - Michael Baudin
function [f,g] = quad ( x )
  H = [
    3 1
    1 3
    ];
  f = 0.5 * x' * H * x
  g = H * x
  plot ( x(1),x(2),"bo")
endfunction
function f = quadC ( x1 , x2 )
  x = [x1 x2]'
  H = [
    3 1
    1 3
    ];
  f = 0.5 * x' * H * x
endfunction

mprintf("Please wait...\n")
f = scf(100001);

n = 60;
x = linspace ( -5, 7 , n );
y = linspace ( -5, 5 , n );
contour ( x , y , quadC ,  [0.2 2 10 20 30 50] )

x0 = [-2 0]';
// Check derivatives
[f,g] = quad ( x0 )
gnd = numderivative ( quad , x0 )

plot(x0(1), x0(2), "r*");
[x,histout,costdata] = optkelley_bfgswopt(x0,quad,1.e-4,100)
plot(x(1), x(2), "go");

//
// Load this script into the editor
//
filename = 'bfgs2.sce';
dname = get_absolute_file_path(filename);
editor ( dname + filename );

