function [x,histout,costdata] = optkelley_cgtrust (varargin)
  // Steihaug Newton-CG-Trust region algorithm.
  //
  // Calling Sequence
  //   [x,histout,costdata] = optkelley_cgtrust(x0,f)
  //   [x,histout,costdata] = optkelley_cgtrust(x0,f,parms)
  //   [x,histout,costdata] = optkelley_cgtrust(x0,f,parms,resolution)
  //
  // Parameters
  //   x0: initial iterate
  //   f: objective function, the calling sequence for f should be [fout,gout]=f(x) where fout=f(x) is a scalar and gout = grad f(x) is a COLUMN vector
  //   parms: (optional) an empty matrix, a 1x1 matrix of doubles containing tol, or a 2x1 matrix of doubles containing [tol, eta], or a 3x1 matrix of doubles containing [tol, eta, maxit], or a 4x1 matrix of doubles containing [tol, eta, maxit, maxitl]. If params=[], then the default is set.
  //   parms(1): tol, termination criterion norm(grad) < tol, default = 1.d-6
  //   parms(2): (optional) eta, forcing term in linear iteration, default = .1
  //   parms(3): (optional) maxit, maximum nonlinear iterations, default = 100
  //   parms(4): (optional) maxitl, maximum linear iterations, default = 20
  //   resolution: (optional) estimated accuracy in functions/gradients (optional), default = 1.d-12. The finite difference increment in the difference Hessian is set to sqrt(resolution). If resolution==[], then the default is set.
  //   verbose: (optional) if true, print messages (default=%f). If verbose==[], the default parameter is used.
  //   x: solution
  //   histout: a niter x 4 matrix of doubles, the iteration history. Each row of histout is [norm(grad), f, TR radius, inner iteration count]
  //   costdata: a 1 x 2 matrix of doubles, [num f, num grad]
  //
  // Description
  // This code comes with no guarantee or warranty of any kind.
  //
  // Requires: optkelley_dirdero.sci
  //
  // Examples
  // function [f,g] = rosenbrock ( x )
  //   f = 100.0 *(x(2)-x(1)^2)^2 + (1-x(1))^2;
  //   g(1) = - 400. * ( x(2) - x(1)**2 ) * x(1) -2. * ( 1. - x(1) )
  //   g(2) = 200. * ( x(2) - x(1)**2 )
  // endfunction
  //
  // // With default parameters
  // x0 = [-1.2 1.0]';
  // [xopt, histout, costdata] = optkelley_cgtrust(x0, rosenbrock);
  // xopt
  // histout
  // costdata
  // niter = size(histout,"r")
  // fopt = histout(niter,2)
  //
  // // Customize parameters
  // // Set tol
  // [xopt, histout, costdata] = optkelley_cgtrust(x0, rosenbrock, 0);
  // // Set tol and eta
  // [xopt, histout, costdata] = optkelley_cgtrust(x0, rosenbrock, [1e-6 0.5]);
  // // Set tol, eta and maxit
  // [xopt, histout, costdata] = optkelley_cgtrust(x0, rosenbrock, [1e-6 0.1 22]);
  // // Set tol, eta, maxit and maxitl
  // [xopt, histout, costdata] = optkelley_cgtrust(x0, rosenbrock, [1e-6 0.1 50 1]);
  // // Use default params and set resolution
  // [xopt, histout, costdata] = optkelley_cgtrust(x0, rosenbrock, [], 1e-12);
  // // Use default params and resolution, set verbose
  // [xopt, histout, costdata] = optkelley_cgtrust(x0, rosenbrock, [], [] , %t );
  // // Configure all parameters
  // [xopt, histout, costdata] = optkelley_cgtrust(x0, rosenbrock, [1e-6 0.1 1000 100], 1e-12 , %t );
  //
  // // Other ways of using the default parameters
  // [xopt, histout, costdata] = optkelley_cgtrust(x0, rosenbrock );
  // [xopt, histout, costdata] = optkelley_cgtrust(x0, rosenbrock , [] );
  // [xopt, histout, costdata] = optkelley_cgtrust(x0, rosenbrock , [] , [] );
  // [xopt, histout, costdata] = optkelley_cgtrust(x0, rosenbrock , [] , [] , [] );
  //
  // Authors
  // C. T. Kelley, Jan 13, 1998
  // Yann Collette, Scilab port
  // Michael Baudin; DIGITEO, 2010, Update to Scilab 5.2, Bug fixes, help formatting, added examples and tests, improved management of input arguments

  //
  [lhs, rhs] = argn();
  if ( rhs < 2 | rhs > 5 ) then
    errmsg = msprintf(gettext("%s: Unexpected number of input arguments : %d provided while 2 to 5 are expected."), "optkelley_cgtrust", rhs);
    error(errmsg)
  end
  //
  x0 = varargin(1)
  f = varargin(2)
  // set tol, eta, maxit and maxitl
  tol = 1.d-6
  eta = .1;
  maxit = 100;
  maxitl = 20;
  if ( rhs >= 3 & varargin(3) <> [] ) then
    parms = varargin(3)
    np = length(parms);
    tol = parms(1)
    if (np >= 2) then
      eta = parms(2);
    end
    if (np >= 3) then
      maxit = parms(3);
    end
    if (np >= 4) then
      maxitl = parms(4);
    end
  end
  resolution = 1.d-12;
  if ( rhs >= 4 & varargin(4) <> [] ) then
    resolution = varargin(4)
  end
  verbose = %f
  if ( rhs >= 5 & varargin(5) <> [] ) then
    verbose = varargin(5)
  end
  //
  _debug = 0;
  //
  omegaup   = 2;
  omegadown = .5;
  mu0       = .25;
  mulow     = .25;
  muhigh    = .75;
  //
  numf  = 0;
  numg  = 0;
  hdiff = sqrt(resolution);
  t   = 100;
  itc = 0;
  x  = x0;
  n   = length(x0);
  [fc,gc] = f(x);
  numf   = 1;
  numg   = 1;
  trrad  = norm(x0);
  ithist = zeros(maxit,4);
  if ( verbose ) then
    mprintf("it=%d, g=%15.5e, f=%15.5e, trrad=%15.5e, itsl=%d\n",itc+1,norm(gc), fc, trrad, 0)
  end
  ithist(itc+1,:) = [norm(gc), fc, trrad, 0];
  //
  paramstr = [eta,maxitl];
  trcount  = 1;
  while (norm(gc) > tol & itc <maxit-1 & trcount < 30)
    itc = itc+1;
    [s,dirs] = trcg (x, f, gc, trrad, paramstr, [] , verbose );
    if ( norm(s) == 0 ) then
      break
    end
    vd   = size(dirs);
    itsl = vd(2);
    numf = numf+itsl;
    numg = numg+itsl;
    xt   = x+s;
    ft   = f(xt);
    numf = numf+1;
    ared = ft-fc;
    w    = optkelley_dirdero(x, s, f, gc);
    numg = numg+1;
    pred = gc'*s + .5*(w'*s);
    _rat  = ared/pred;
    if (_rat > mu0) then
      x = x+s;
      [fc,gc] = f(x);
      numf = numf+1;
      if (_rat > muhigh) & (norm(s) > trrad-1.d-8) then
        trrad = omegaup*trrad;
      end
      if (_rat < mulow) then
        trrad = omegadown*trrad;
      end
    else
      for k=1:itsl
        dsums(k) = norm(mtlb_sum(dirs(:,1:k),2));
      end
      trcount = 1;
      while ((_rat <= mu0) & (trcount < 30))
        trrad = omegadown*min(trrad,norm(s));
        [s, kout] = tradj(trrad, dirs, itsl);
        xt   = x+s;
        ft   = f(xt);
        numf = numf+1;
        ared = ft-fc;
        //
        //      Only compute a new pred if ared < 0 and there's some hope
        //      that rat > mu0
        //
        if (ared < 0) then
          w    = optkelley_dirdero(x, s, f, gc);
          numg = numg+1;
          pred = gc'*s + .5*(w'*s);
        end
        _rat     = ared/pred;
        itsl    = kout;
        trcount = trcount+1;
        if (trcount > 30) then
          if ( verbose ) then
            mprintf("%s: stagnation in CGTR\n","optkelley_cgtrust")
          end
        end
      end
      if (trcount < 30) then
        x      = xt;
        [fc,gc] = f(x);
        numf    = numf+1;
        numg    = numg+1;
      end
    end
    if ( verbose ) then
      mprintf("it=%d, g=%15.5e, f=%15.5e, trrad=%15.5e, itsl=%d\n",itc+1,norm(gc), fc, trrad, itsl)
    end
    ithist(itc+1,:) = [norm(gc), fc, trrad, itsl];
  end
  histout  = ithist(1:itc+1,:);
  costdata = [numf,numg];
endfunction
function [st, kout] = tradj (trrad, dirs, itsl)
  // find the point of intersetion of the TR boundary and the PL path
  //

  st     = dirs(:,1);
  inside = 1;
  if (norm(st) > trrad) | (itsl == 1) then
    st   = st*trrad/norm(st);
    kout = 1;
  else
    for k=2:itsl
      if ((norm(st+dirs(:,k)) > trrad) & (inside == 1)) then
        kout   = k;
        p      = dirs(:,k);
        ac     = p'*p;
        bc     = 2*(st'*p);
        cc     = st'*st - trrad*trrad;
        alpha  = (-bc + sqrt(bc*bc - 4*ac*cc))/(2*ac);
        st     = st+alpha*p;
        inside = 0;
      else
        st = st+dirs(:,k);
      end
    end
  end
endfunction
//
//
//
function [x, directions]  = trcg (xc, f, gc, delta, params, pcv, verbose )
  //
  // Solve the trust region problem with preconditioned conjugate-gradient
  //
  // Calling Sequence
  // [x, directions]  = trcg (xc, f, gc, delta, params, pcv, verbose )
  //
  //
  // Parameters
  //   xc : current point,
  //   f : function, the calling sequence is [fun,grad]=f(x)
  //   gc : current gradient, gc has usually been computed before the call to optkelley_dirdero
  //   delta : TR radius
  //   params : two dimensional vector to control iteration
  //   params(1) : relative residual reduction factor
  //   params(2) : max number of iterations
  //   pcv : a routine to apply the preconditioner. If pcv=[], the identity is used. The format for pcv is function px = pcv(x).
  //   verbose: if true, print messages
  //   x : trial step
  //   directions : array of search directions TR radius reduction
  //
  // Description
  // This code comes with no guarantee or warranty of any kind.
  //
  // Author
  // C. T. Kelley, January 13, 1997
  //

  //
  [lhs, rhs] = argn();
  if ( rhs <> 7 ) then
    errmsg = msprintf(gettext("%s: Unexpected number of input arguments : %d provided while 7 are expected."), "trcg", rhs);
    error(errmsg)
  end
  //
  n = length(xc);
  errtol   = params(1);
  maxiters = params(2);
  x = zeros(n,1);
  b = -gc;
  r = b
  if ( pcv == [] ) then
    z = r;
  else
    z = pcv(r);
  end
  rho        = z'*r;
  tst        = norm(r);
  terminate  = errtol*norm(b);
  it         = 1;
  directions = zeros(n,1);
  hatdel     = delta*(1-1.d-6);
  while((tst > terminate) & (it <= maxiters) & norm(x) <= hatdel)
    if (it==1) then
      p = z;
    else
      _beta = rho/rhoold;
      p = z + _beta*p;
    end
    w     = optkelley_dirdero(xc, p, f, gc);
    alpha = p'*w;
    //
    // If alpha <=0 head to the TR boundary and return
    //
    ineg = 0;
    if (alpha <= 0) then
      ac    = p'*p;
      bc    = 2*(x'*p);
      cc    = x'*x - delta*delta;
      alpha = (-bc + sqrt(bc*bc - 4*ac*cc))/(2*ac);
      if ( verbose ) then
        mprintf("%s: negative curvature\n","optkelley_cgtrust")
      end
    else
      alpha = rho/alpha;
      if (norm(x+alpha*p) > delta) then
        ac    = p'*p;
        bc    = 2*(x'*p);
        cc    = x'*x - delta*delta;
        alpha = (-bc + sqrt(bc*bc - 4*ac*cc))/(2*ac);
      end
    end
    x = x+alpha*p;
    directions(:,it) = alpha*p;
    r      = r - alpha*w;
    tst    = norm(r);
    rhoold = rho;
    if ( pcv == [] ) then
      z=r;
    else
      z = pcv(r);
    end
    rho = z'*r;
    it  = it+1;
  end
endfunction

