// Copyright (C) 2010 - DIGITEO - Michael Baudin

function [f,g] = quad ( x )
  H = [
    3 1
    1 3
    ];
  f = 0.5 * x' * H * x
  g = H * x
  plot ( x(1),x(2),"bo")
endfunction
function f = quadC ( x1 , x2 )
  x = [x1 x2]'
  H = [
    3 1
    1 3
    ];
  f = 0.5 * x' * H * x
endfunction

mprintf("Please wait...\n")
f = scf(100001);

n = 60;
x = linspace ( -2, 2 , n );
y = linspace ( -2, 2 , n );
contour ( x , y , quadC ,  [0.2  0.6  1 2 4 6] )

x0 = [-2 0]';
// Check derivatives
[f,g] = quad ( x0 )
gnd = numderivative ( quad , x0 )

[x,histout,costdata] = optkelley_steep(x0,quad,1.e-4,100)
//
// Load this script into the editor
//
filename = 'steep2.sce';
dname = get_absolute_file_path(filename);
editor ( dname + filename );

