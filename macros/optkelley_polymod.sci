function [lplus]=optkelley_polymod(q0, qp0, lamc, qc, blow, bhigh, lamm, qm)
// Cubic/quadratic polynomial linesearch.
//
// Calling Sequence
//   [lambda]=optkelley_polymod(q0, qp0, lamc, qc, blow, bhigh)
//   [lambda]=optkelley_polymod(q0, qp0, lamc, qc, blow, bhigh, lamm, qm)
//
// Parameters
//   q0: function value at lambda=0, q(0) = q0
//   qp0: value of gradient at lambda=0, q'(0) = qp0
//   lamc: current value of lambda
//   qc: function value at lamc, q(lamc) = qc
//   blow: factor of the lower bound for lambda
//   bhigh: factor of the upper bound for lambda
//   lamm: another interpolation point
//   qm: value of function at lamm
//   lplus: value of lambda achieving the minimum of the polynomial model
//
// Description
// If there are 8 input arguments, finds minimizer lambda of the cubic polynomial q on the interval
// [blow * lamc, bhigh * lamc] such that
// q(0) = q0, q'(0) = qp0, q(lamc) = qc, q(lamm) = qm
// If there are 6 input arguments, that is, if data for a cubic is not available (first stepsize reduction) then
// q is the quadratic such that
// q(0) = q0, q'(0) = qp0, q(lamc) = qc
//
// This code comes with no guarantee or warranty of any kind.
//
// Authors
// C. T. Kelley, Dec 29, 1997
// Yann Collette, Scilab port
// Michael Baudin, DIGITEO, 2010, Update to Scilab 5.2, formatted comments
//

[nargout, nargin] = argn();

lleft  = lamc*blow;
lright = lamc*bhigh;

if (nargin == 6) then
  //
  // quadratic model (temp hedge in case lamc is not 1)
  //
  lplus = - qp0/(2 * lamc*(qc - q0 - qp0) );
  if (lplus < lleft) then lplus = lleft; end
  if (lplus > lright) then lplus = lright; end
else
  //
  // cubic model
  //
  a = [lamc^2, lamc^3; lamm^2, lamm^3];
  b = [qc; qm]-[q0 + qp0*lamc; q0 + qp0*lamm];
  c = a\b;
  lplus = (-c(1)+sqrt(c(1)*c(1) - 3 *c(2) *qp0))/(3*c(2));
  if (lplus < lleft) then lplus = lleft; end
  if (lplus > lright) then lplus = lright; end
end
endfunction
