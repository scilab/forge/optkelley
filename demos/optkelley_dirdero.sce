mode(1)
//
// Demo of optkelley_dirdero.sci
//

function [f,g] = rosenbrock ( x )
f = 100.0 *(x(2)-x(1)^2)^2 + (1-x(1))^2;
g(1) = - 400. * ( x(2) - x(1)**2 ) * x(1) -2. * ( 1. - x(1) )
g(2) = 200. * ( x(2) - x(1)**2 )
endfunction
function H = roseHess ( x )
H(1,1) = 1200 * x(1)**2 - 400 * x(2) + 2
H(1,2) = -400 * x(1)
H(2,1) = H(1,2)
H(2,2) = 200
endfunction
//
// Test with default epsnew
x0 = [-1.2 1.0]';
w = [1 0]';
[f0,g0] = rosenbrock ( x0 );
z = optkelley_dirdero(x0, w, rosenbrock, g0);
// Compare with the exact value
roseHess ( x0 ) * w
// Compare with optimal step (assuming f and f'' are of order unity)
z = optkelley_dirdero(x0, w, rosenbrock, g0, sqrt(%eps));
halt()   // Press return to continue

//========= E N D === O F === D E M O =========//
//
// Load this script into the editor
//
filename = "optkelley_dirdero.sce";
dname = get_absolute_file_path(filename);
editor ( dname + filename );
