mode(1)
//
// Demo of optkelley_nelder.sci
//

function y = rosenbrock ( x )
y = 100*(x(2)-x(1)^2)^2 + (1-x(1))^2;
endfunction
x0 = [-1.2 1]';
v1 = x0 + [0.1 0]';
v2 = x0 + [0 0.1]';
v0 = [x0 v1 v2];
[x,lhist,histout,simpdata] = optkelley_nelder(v0,rosenbrock,1.e-4,1000,1000);
xopt = x(:,1)
fopt = histout(lhist,2)
halt()   // Press return to continue

//========= E N D === O F === D E M O =========//
//
// Load this script into the editor
//
filename = "optkelley_nelder.sce";
dname = get_absolute_file_path(filename);
editor ( dname + filename );
